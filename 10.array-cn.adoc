= 数组

[source]
----
var a = ["hello", "world", "!"]
----

数组使用方括号来定义，空数组即没有元素在里面。

== *注意*

1. 数组元素仅支持同类型元素
2. 数组元素类型只限 `string` 、 `digital` 、 `boolean` 、 `nil` 、 `array` 、 `map`
3. lambda表达式、函数不适合作为数组元素
