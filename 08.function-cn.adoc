= 函数

[source]
----
func foo() {
  print("foo!")
}
----

定义一个函数包括func关键字、函数名、参数列表和执行语句构成。`foo` 就是函数名，参数列表为空，执行语句是打印 `foo!` 。

== 带参函数

[source]
----
func plus(var a, var b) {
  print(a+b)
}

plus(1,2)
----
这是一个计算加法的函数，参数有两个 `a` 和 `b` 。计算并打印 `1+2` 的结果。

== 带默认值函数

[source]
----
func plus(var a = 1, var b) {
  print(a+b)
}

plus(2) // 同
plus(nil, 2)
----
如果一个参数在圆括号里这么写，这代表这个参数有默认值。调用时如果为 `nil` 或者直接不写则使用默认值。
这个函数计算并打印 `1+2` 的结果。

== 带返回函数

[source]
----
func plus(var a = 1, var b) {
  return a+b
}

var count = plus(nil,2)
print("This count is " + count + ".")
----

这里加入了 `return` 关键字，同理还是计算 `a+b` ，为了方便理解加入了 `count` 变量。 `print`
函数内的加号为字符串连接符。关于字符串会在相关章节讲解。

== 匿名函数

请参考Lambda章节

== `return`

`return` 关键字用于函数返回，后面可接多个变量。是的NJES支持多返回，用逗号分割多个变量。

在 `return` 之后不带有任何表达式可作为 `continue` 使用，或者返回最近的块（逻辑块会直接退出所在逻辑结构）。

一个函数可以 `return` 至少一个任意表达式。但是为了保持结构简洁性，请不要返回 *另一个函数*，
我们坚决不建议你在 *函数内套函数* ，即使支持这种写法。
